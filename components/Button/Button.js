import styles from './Button.module.scss'
import Icon from "../Icon/Icon";
export default function Button(
  {
    type= 'button',
    label= '',
    color= 'primary',
    size= 'md',
    icon= '',
    iconSize= 26,
    href,
    target="_self",
    title,
    border = true,
    block = false,
    className,
    ...props
  }) {
  if (href) return(
    <a
      className={`${styles['ll_btn']} ${styles[`ll_btn--${color}`]} ${styles[`ll_btn--${size}`]} ${border ? 'll_btn--border' : ''} ${block ? 'll_btn--block' : ''} ${ className ? className : '' }`}
      href={ href }
      title={ title }
      target={ target }
    >
      { icon && <span className={ styles.ll_btn__icon }><Icon iconName={ icon } size={ iconSize } /></span> }
      { label && <span>{ label }</span> }
    </a>
  )
  return(
    <button
      className={`${styles['ll_btn']} ${styles[`ll_btn--${color}`]} ${styles[`ll_btn--${size}`]} ${border ? 'll_btn--border' : ''} ${ className ? className : '' }`}
      type={ type }
    >
      { icon && <span className={ styles.ll_btn__icon }><Icon iconName={ icon } size={ iconSize } /></span> }
      { label && <span>{ label }</span> }
    </button>
  )
}