import styles from './Footer.module.scss';
import FooterInfo from "./FooterInfo/FooterInfo";
import FooterCredit from "./FooterCredit/FooterCredit";
import FooterSocial from "./FooterSocial/FooterSocial";

export default function Footer({ footer, menu, social, siteTitle }) {
  return (
    <footer className={ styles.ll_footer }>
      { /* INFO */ }
      <FooterInfo footer={ footer } menu={ menu } />
      { /* SOCIAL */ }
      <FooterSocial social={ social } />
      { /* CREDIT */ }
      <FooterCredit siteTitle={ siteTitle } />
    </footer>
  )
}