import styles from './FooterCredit.module.scss'
export default function FooterCredit({ siteTitle='Lalli Antonio' }) {
  const today = new Date();
  const year = today.getFullYear()
  return(
    <section className={ styles.ll_footer_credit }>
      <div className={ styles.ll_container }>
        <span>{`© ${ siteTitle } - ${ year }`}</span>
        <span>Designed and Developed <b>❤️</b> <a href="https://giulianosalvatore.com" title="Giuliano Salvatore | UX/UI Designer | Front End Developer ">Giuliano Salvatore</a></span>
      </div>
    </section>
  )
}