import styles from './FooterInfo.module.scss'
import Icon from "../../Icon/Icon";
export default function FooterInfo({ footer, menu }) {
  return(
    <section className={ styles.ll_footer_info }>
      <div className={ styles.ll_container }>
        { /* ADDRESS */ }
        {
          footer.address && (
            <article className={ styles.ll_footer_article }>
              <header>
                <h5>{ footer.address.title }</h5>
              </header>
              <ul className={ styles.ll_footer_article__list }>
                <li>
                  <h6><b>{ footer.address.name }</b></h6>
                </li>
                <li>
                  <h6>{ footer.address.owner }</h6>
                </li>
              </ul>
              <ul className={ styles.ll_footer_article__list }>
                <li>
                  <h6 dangerouslySetInnerHTML={{__html: footer.address.address}} />
                </li>
              </ul>
            </article>
          )
        }
        { /* CONTACTS */ }
        {
          footer.contacts && (
            <article className={ styles.ll_footer_article }>
              <header>
                <h5>{ footer.contacts.title }</h5>
              </header>
              <ul className={ styles.ll_footer_article__list }>
                <li>
                  <h6>
                    <span className={ styles.crs_label }>Tel. <b>•</b> </span>
                    <span className={ styles.crs_value }>{ footer.contacts.phone }</span>
                  </h6>
                </li>
                <li>
                  <h6>
                    <span className={ styles.crs_label }>Cel. <b>•</b> </span>
                    <span className={ styles.crs_value }>{ footer.contacts.mobile }</span>
                  </h6>
                </li>
                <li>
                  <h6>
                    <span className={ styles.crs_label }>Email <b>•</b> </span>
                    <span className={ styles.crs_value }>{ footer.contacts.email }</span>
                  </h6>
                </li>
              </ul>
            </article>
          )
        }
        { /* TIMETABLES */ }
        {
          footer.timetables && (
            <article className={ styles.ll_footer_article }>
              <header>
                <h5>{ footer.timetables.title }</h5>
              </header>
              <ul className={ styles.ll_footer_article__list }>
                <li>
                  <h6>{ footer.timetables.day }</h6>
                </li>
                <li>
                  <h6>{ footer.timetables.night }</h6>
                </li>
              </ul>
            </article>
          )
        }
        { /* MENU */ }
        {
          menu && (
            <article className={ styles.ll_footer_article }>
              <header>
                <h5>Naviga</h5>
              </header>
              <nav>
                <ul className={ styles.ll_footer_article__list }>
                  { Object.keys(menu).map((item, idx) => (
                      <li key={ idx }>
                        <a href="#" title={ menu[item].label } className={ styles.ll_footer_nav__item }>
                          <span className={ styles.ll_icon }>
                            <Icon iconName={ menu[item].icon } size={ 24 } />
                          </span>
                          <span>{ menu[item].label }</span>
                        </a>
                      </li>
                    )
                  )}
                </ul>
              </nav>
            </article>
          )
        }
      </div>
    </section>
  )
}