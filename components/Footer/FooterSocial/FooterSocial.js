import Image from "next/image";
import Button from "../../Button/Button";
import styles from './FooterSocial.module.scss'
export default function FooterSocial({ social }) {
  if (!social) return;
  return(
    <section className={ styles.ll_footer_social }>
      <div className="ll_container">
        <a href="#">
          <Image
            src="/locanda_lalli_logo.svg"
            alt="La locanda del buongustaio | Locanda Lalli"
            width={ 150 }
            height={ 70 }
          />
        </a>
        {
          social && (
            <nav className={ styles.ll_footer_social_nav }>
              {
                Object.keys(social).map((item, idx) => {
                  if (social[item]) {
                    const socialBlock = social[item]
                    return (
                      <Button
                        color="secondary"
                        href={ socialBlock.url }
                        target={ socialBlock.target }
                        title={ `${socialBlock.title} | La Locanda del Buongustaio | Locanda Lalli ` }
                        icon={ item }
                        border={ false }
                        key={ idx } />
                    )
                  }
                })
              }
            </nav>
          )
        }
      </div>
    </section>
  )
}