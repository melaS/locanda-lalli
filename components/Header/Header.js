import styles from './Header.module.scss'
import Image from 'next/image'
import Icon from "../Icon/Icon";
export default function Header({ menu, phone, header }) {
  const siteTitle = header.siteName;
  if (!menu) return;
  return(
    <header className={ styles.ll_header }>
      <div className={ styles.ll_container }>
        { /* NAV - ROOMS | RESTAURANT | TAKEAWAY */ }
        {
          menu && (
            <nav className={ styles.ll_header_nav }>
              { Object.keys(menu).map((item, idx) => {
                if (idx < 3 ) {
                  return (
                    <a
                      key={idx}
                      href="#"
                      title={`${menu[item].label} | ${ siteTitle } | Locanda Lalli`}
                      className={styles.ll_header_nav__item}
                    >
                    <span className={styles.ll_icon}>
                      <Icon iconName={menu[item].icon}/>
                    </span>
                      <span className={styles.ll_text}>{menu[item].label}</span>
                    </a>
                  )
                }
              }
            )}
          </nav>
          )
        }

        { /* LOGO */ }
        <a href="/" className={ styles.ll_header_logo } title={`${ siteTitle } | Locanda Lalli`}>
          <Image
            src="/locanda_lalli_logo.svg"
            alt={ `${ siteTitle } | Locanda Lalli`}
            width={ 150 }
            height={ 70 }
          />
        </a>
        { /* NAV - LOCATION | PHONE */ }
        <nav className={ styles.ll_header_nav }>
          {
            menu['item4'] && (
              <a className={styles.ll_header_nav__item} href="#" title={`${menu['item4'].label} | ${ siteTitle } | Locanda Lalli`}>
                <span className={ styles.ll_icon }>
                  <Icon iconName={ menu['item4'].icon } />
                </span>
                <span className={styles.ll_text}>{ menu['item4'].label }</span>
              </a>
            )
          }
          {
            phone.number && (
              <a className={styles.ll_header_nav__item} href={`tel:${phone.number}`} title={`tel: ${phone.number} | ${ siteTitle } | Locanda Lalli`}>
                <span className={ styles.ll_icon }>
                  <Icon iconName="phone" />
                </span>
                <span className={styles.ll_text}>{ phone.number }</span>
              </a>
            )
          }
        </nav>
      </div>
    </header>
  )
}