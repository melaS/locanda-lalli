import React, { useState, useEffect } from 'react';
import styles from './Icon.module.scss'

export default function Icon({ iconName = 'room', size= 24 }) {

  const [path, setPath] = useState('')

  useEffect(() => {
    const fetchIcons = async () => {
      const jsonIcons = await import('../../lib/icons/icons_generic.json')
      const path = jsonIcons.icons.find(icon => icon.tags.some(tag => tag === iconName) );
      if (path) setPath(path.paths[0])
    }
    fetchIcons()

  }, [iconName])
  return(
    <svg
      className={ styles.ll_svg_icon }
      viewBox="0 0 1024 1024"
      preserveAspectRatio="xMidYMid meet"
      width={  size }
      height={ size }
    >
      <path d={ path } />
    </svg>
  )
}