import styles from './LandingAbout.module.scss'
export default function LandingAbout({ about }) {
  if (!about) return;
  return(
    <section className={ styles.ll_about }>
      {
        about.image.sourceUrl && (
          <figure className={ styles.ll_about_image }>
            <img src={ about.image.sourceUrl } alt={ about.image.altText } />
          </figure>
        )
      }
      <article className={ styles.ll_about_article }>
        <header>
          <h3>{ about.title }</h3>
          <h4>{ about.subtitle }</h4>
        </header>
        <div className={ styles.ll_about_article__content }>
          <div dangerouslySetInnerHTML={{__html: about.description}}></div>
        </div>
      </article>
    </section>
  )
}