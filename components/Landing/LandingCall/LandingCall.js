import Button from "../../Button/Button";
import styles from './LandingCall.module.scss'
export default function LandingCall({ call, telephoneNumber, header }) {
  return(
    <section className={ styles.ll_call }>
      <header>
        <h4>{ call.title }</h4>
      </header>
      <div className={ styles.ll_call__content }>
        <div dangerouslySetInnerHTML={{__html: call.description}}></div>
        <Button
          className={ styles.ll_call_phone }
          href={ telephoneNumber.number }
          color="secondary"
          icon="phone"
          label={ telephoneNumber.number }
          size="lg"
          title={`Chiama allo ${ telephoneNumber.number } | La Locanda del Buongustaio | Locanda Lalli `}
        />
      </div>
    </section>
  )
}