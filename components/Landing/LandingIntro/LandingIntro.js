import Image from "next/image";
import styles from './LandingIntro.module.scss'
export default function LandingIntro({ intro }) {
  const imgUrl =  intro.image.sourceUrl
  return(
    <div className={styles.ll_intro}>
      <header className={ styles.ll_intro_title }>
        <h1>{ intro.title }</h1>
        <h2>{ intro.subtitle }</h2>
      </header>
      <figure className={ styles.ll_intro_figure }>
        <Image loader={() => imgUrl} src={ imgUrl } alt="Locanda del Buon Gustaio | Di Lalli Antonio" layout="fill" />
      </figure>
    </div>

  )
};
