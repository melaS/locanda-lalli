import Map from "../../common/Map/Map";
import Button from "../../Button/Button";
import styles from './LandingLocation.module.scss'
export default function LandingLocation({ location }) {
  return(
    <section className={ styles.ll_location }>
      <article className={ styles.ll_location_article }>
        <header>
          <h3>{ location.title }</h3>
        </header>
        <div className={ styles.ll_location_article__content }>
          <div dangerouslySetInnerHTML={{__html: location.address}}></div>
          {
            (location.contact.phone || location.contact.email) && (
              <ul>
                {
                  location.contact.phone && (
                    <li>
                      <span>Tel: <a href={`tel:${location.contact.phone}`} target="_blank" title={`Tel: ${location.contact.phone} | La Locanda del Buongustaio | Locanda Lalli`}>{ location.contact.phone }</a></span>
                    </li>
                  )
                }
                {
                  location.contact.email && (
                    <li>
                      <span>Email: <a href={`email:${location.contact.email}`} target="_blank" title={`Email: ${location.contact.email} | La Locanda del Buongustaio | Locanda Lalli`}>{ location.contact.email }</a></span>
                    </li>
                  )
                }
              </ul>
            )
          }
        </div>
        <footer className={ styles.ll_location_article__footer }>
          {
            location.link && (
              <Button
                href={location.link.url}
                label={ location.link.title }
                title={ location.link.title }
                color="secondary"
                size="lg"
                block={ true }
              />
            )
          }
        </footer>
      </article>
      <div className={ styles.ll_location_map }>
        <Map zoom={ 19 } />
      </div>
    </section>
  )
}