import SectionBlock from "../../common/SectionBlock/SectionBlock";
import SectionBlockLeft from "../../common/SectionBlock/SectionBlockLeft/SectionBlockLeft";
import SectionBlockRight from "../../common/SectionBlock/SectionBlockRight/SectionBlockRight";
import SectionBlockArticle from "../../common/SectionBlock/SectionBlockArticle/SectionBlockArticle";
import SectionBlockArticleFooter from "../../common/SectionBlock/SectionBlockArticle/SectionBlockArticleFooter";
import SectionBlockGallery from "../../common/SectionBlock/SectionBlockGallery/SectionBlockGallery";
import SectionBlockPrice from "../../common/SectionBlock/SectionBlockPrice/SectionBlockPrice";
import styles from './LandingRestaurant.module.scss'
import Button from "../../Button/Button";
export default function LandingRestaurant({ restaurant, telephoneNumber }) {
  if (!restaurant) return;
  return(
    <SectionBlock className={ styles.ll_restaurant }>
      <SectionBlockLeft>
        <SectionBlockArticle title={ restaurant.title } subtitle={ restaurant.subtitle }>
          <div dangerouslySetInnerHTML={{__html: restaurant.description}}></div>
          <SectionBlockArticleFooter>
            <SectionBlockPrice price={ restaurant.price } />
            <Button
              color="secondary"
              label={ telephoneNumber.labelBook }
              border={ true }
              size="lg"
              href={ `tel:${telephoneNumber.number}`}
              title={`${telephoneNumber.labelBook} | ${ telephoneNumber.number }`}
            />
          </SectionBlockArticleFooter>
        </SectionBlockArticle>
      </SectionBlockLeft>
      <SectionBlockRight>
        <SectionBlockGallery gallery={ restaurant.gallery } />
      </SectionBlockRight>
    </SectionBlock>
  )
}