import Badge from "../../common/Badge/Badge";
import Button from "../../Button/Button";
import SectionBlock from "../../common/SectionBlock/SectionBlock";
import SectionBlockLeft from "../../common/SectionBlock/SectionBlockLeft/SectionBlockLeft";
import SectionBlockRight from "../../common/SectionBlock/SectionBlockRight/SectionBlockRight";
import SectionBlockArticle from "../../common/SectionBlock/SectionBlockArticle/SectionBlockArticle";
import SectionBlockArticleFooter from "../../common/SectionBlock/SectionBlockArticle/SectionBlockArticleFooter";
import SectionBlockGallery from "../../common/SectionBlock/SectionBlockGallery/SectionBlockGallery";
import SectionBlockPrice from "../../common/SectionBlock/SectionBlockPrice/SectionBlockPrice";
import styles from './LandingRooms.module.scss'
export default function LandingRooms({ rooms, telephoneNumber }) {
  if(!rooms) return;
  return(
    <SectionBlock className={ styles.ll_rooms }>
      <SectionBlockLeft>
        <SectionBlockGallery gallery={ rooms.gallery } />
      </SectionBlockLeft>
      <SectionBlockRight>
        <SectionBlockArticle title={ rooms.title } subtitle={ rooms.subtitle }>
          <div dangerouslySetInnerHTML={{__html: rooms.description}}></div>
          {
            rooms.services && (
              <ul className="ll_badge_list">
                {
                  Object.keys(rooms.services).map((key, idx) => {
                    return(
                      <li key={ idx }>
                        <Badge iconName={ rooms.services[key] } label={ rooms.services[key] } color="secondary" />
                      </li>
                    )
                  })
                }
              </ul>
            )
          }
          <SectionBlockArticleFooter>
            <SectionBlockPrice price={ rooms.price } />
            <Button color="secondary" label={ telephoneNumber.labelBook } border={ true } size="lg" href={ `tel:${telephoneNumber.number}`} title={`${telephoneNumber.labelBook} | ${ telephoneNumber.number }`} />
            <div className={ styles.ll_rooms__rates }>
              <span>{ rooms.priceList.title }</span>
              <div dangerouslySetInnerHTML={{__html: rooms.priceList.prices}}></div>
            </div>
          </SectionBlockArticleFooter>
        </SectionBlockArticle>
      </SectionBlockRight>
    </SectionBlock>

  )
}