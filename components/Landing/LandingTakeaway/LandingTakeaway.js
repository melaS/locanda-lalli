import Badge from "../../common/Badge/Badge";
import Button from "../../Button/Button";
import SectionBlock from "../../common/SectionBlock/SectionBlock";
import SectionBlockLeft from "../../common/SectionBlock/SectionBlockLeft/SectionBlockLeft";
import SectionBlockRight from "../../common/SectionBlock/SectionBlockRight/SectionBlockRight";
import SectionBlockArticle from "../../common/SectionBlock/SectionBlockArticle/SectionBlockArticle";
import SectionBlockArticleFooter from "../../common/SectionBlock/SectionBlockArticle/SectionBlockArticleFooter";
import SectionBlockGallery from "../../common/SectionBlock/SectionBlockGallery/SectionBlockGallery";
import SectionBlockPrice from "../../common/SectionBlock/SectionBlockPrice/SectionBlockPrice";
import styles from './LandingTakeaway.module.scss'
export default function LandingTakeaway({ takeaway, telephoneNumber }) {
  return(
    <SectionBlock className={ styles.ll_takeaway }>
      <SectionBlockLeft>
        <SectionBlockGallery gallery={ takeaway.gallery } />
      </SectionBlockLeft>
      <SectionBlockRight>
        <SectionBlockArticle title={ takeaway.title } subtitle={ takeaway.subtitle }>
          <div dangerouslySetInnerHTML={{__html: takeaway.description}}></div>
          {
            takeaway.services && (
              <ul className="ll_badge_list">
                {
                  Object.keys(takeaway.services).map((key, idx) => {
                    return(
                      <li key={ idx }>
                        <Badge iconName={ takeaway.services[key] } label="Consegna a Domicilio" color="secondary" />
                      </li>
                    )
                  })
                }
              </ul>
            )
          }
          <SectionBlockArticleFooter>
            <SectionBlockPrice price={ takeaway.price } from={ false } />
            <Button
              color="secondary"
              label={ telephoneNumber.labelOrder }
              border={ true }
              size="lg"
              href={ `tel:${telephoneNumber.number}`}
              title={`${telephoneNumber.labelBook} | ${ telephoneNumber.number }`}
            />
          </SectionBlockArticleFooter>
        </SectionBlockArticle>
      </SectionBlockRight>
    </SectionBlock>
  )
}