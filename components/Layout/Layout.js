import Meta from "../common/Meta";
import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import styles from './Layout.module.scss'
export default function Layout({ menu, header, footer, telephone, social, children }) {
  return(
    <>
      <Meta />
      <Header menu={ menu } phone={ telephone } header={ header } />
      <main className={ `${ styles.ll_main ? styles.ll_main : '' } ${ styles.ll_container }` }>{ children }</main>
      <Footer footer={ footer } menu={ menu } social={ social } siteTitle={ header.siteName } />
    </>
  )
}