import Icon from "../../Icon/Icon";
import styles from './Badge.module.scss'
export default function Badge({ iconName="wi-fi", label="label", color='secondary' }) {
  return(
    <div className={ `${styles.ll_badge} ${ color ? `ll_badge--${color}` : '' }` }>
      <span className={ styles.ll_badge__icon }>
        <Icon iconName={ iconName } size={ 44 } />
      </span>
      { label && <span className={ styles.ll_badge__text }>{ label }</span> }
    </div>
  )
}