import GoogleMapReact from 'google-map-react';
import Marker from "./Marker";
export default function Map({ lat=41.70400839027932, lng=14.937302311729171, zoom= 10 }) {
  const center = {
    lat,
    lng
  }
  return(
    <GoogleMapReact
      bootstrapURLKeys="AIzaSyChzgxQW01Y1RatoLbLVfZkzwPqnHmVm58"
      defaultCenter={ center }
      defaultZoom={ zoom }
    >
      <Marker
        lat={ lat }
        lng={ lng }
        text="La locanda del Buongustaio"
      />
    </GoogleMapReact>
  )
}