import Icon from "../../Icon/Icon";
import styles from './Marker.module.scss'
export default function Marker({ text }) {
  return(
    <div className={ styles.ll_marker }>
      <span className={ styles.ll_marker__icon }>
        <Icon iconName="location" size={40} />
      </span>
      <div className={ styles.ll_marker__content }>
        <span>{ text }</span>
      </div>
    </div>
  )
}