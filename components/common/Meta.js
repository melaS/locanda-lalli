import Head from "next/head";
export default function Meta() {
  return(
    <Head>
      <meta charSet="UTF-8" />
      <meta name="author" content="Salvatore Giuliano" />
      <meta name="theme-color" content="#000" />
      <meta name="msapplication-TileColor" content="#000000" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
      <link rel="preconnect" href="https://fonts.gstatic.com" />
      {/*<link href="https://fonts.googleapis.com/css2?family=Lato:wght@100;300;400;700;900&family=Martel:wght@200;300;400;600;700;800;900&display=swap" rel="stylesheet" />*/}
    </Head>
  )
}