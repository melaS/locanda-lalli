import styles from './SectionBlock.module.scss'
export default function SectionBlock({ className, children }) {
  return(
    <section className={`${styles.ll_block} ${ className ? className : '' }`}>
      { children }
    </section>
  )
}