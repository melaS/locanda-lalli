import { useRef, useEffect } from 'react'
import { gsap } from 'gsap/dist/gsap';
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import styles from './SectionBlockArticle.module.scss'
export default function SectionBlockArticle({ title, subtitle, children }) {

  const articleRef = useRef(null);

  useEffect(() => {
    const article = articleRef.current;
    gsap.to(article, {
      yPercent: -40,
      scrollTrigger: {
        trigger: article,
        scrub: 1
      }
    })
  })

  return(
    <article className={ styles.ll_block_article } ref={ articleRef }>
      <header className={ styles.ll_block_article__title }>
        <h3>
          <span className={ styles.ll_subtitle }>{ subtitle }</span>
          <span>{ title }</span>
        </h3>
      </header>
      <div className={ styles.ll_block_article__content }>
        { children }
      </div>
    </article>
  )
}