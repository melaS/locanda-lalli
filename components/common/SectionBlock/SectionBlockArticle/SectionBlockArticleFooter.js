import styles from './SectionBlockArticleFooter.module.scss'
export default function SectionBlockArticleFooter({ children }) {
  return(
    <footer className={ styles.ll_block_article__footer }>
      { children }
    </footer>
  )
}