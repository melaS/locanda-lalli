import { useRef, useEffect } from 'react'
import { gsap } from 'gsap/dist/gsap';
import { Swiper, SwiperSlide } from 'swiper/react'
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import Image from "next/image";
import styles from './SectionBlockGallery.module.scss'
import SwiperCore, { EffectFade } from 'swiper/core';
// install Swiper modules
SwiperCore.use([EffectFade]);

export default function SectionBlockGallery({ gallery }) {
  const verticalRef = useRef(null);
  const horizontalRef = useRef(null);
  gsap.registerPlugin(ScrollTrigger);

  useEffect(() => {
    const horizontalGallery = horizontalRef.current;
    gsap.to(horizontalGallery, {
      yPercent: 3,
      scrollTrigger: {
        trigger: horizontalGallery,
        scrub: 1,
      }
    })

  })

  useEffect(() => {
    const verticalGallery = verticalRef.current;
    gsap.to(verticalGallery, {
      yPercent: 0,
      scrollTrigger: {
        trigger: verticalGallery,
        scrub: 1,
      }
    })
  })

  return(
    <div className={ styles.ll_block_gallery }>
      <div className={styles.ll_block_gallery_vertical} ref={ verticalRef }>
        <Swiper
          slidesPerView={1}
          effect={'fade'}
        >
          {
            Object.keys(gallery).map((key, idx) => {
              if (gallery[key].verticalImage && gallery[key].verticalImage.sourceUrl) {
                const imgUrl = gallery[key].verticalImage.sourceUrl
                return (
                  <SwiperSlide key={idx}>
                    <figure className={styles.ll_gallery_image }>
                      <figcaption className={ styles.ll_gallery_title }>{gallery[key].verticalImage.altText}</figcaption>
                      <Image loader={ () => imgUrl } src={ imgUrl } alt={gallery[key].verticalImage.altText} layout="fill" />
                    </figure>
                  </SwiperSlide>
                )
              }
            })
          }
        </Swiper>
      </div>
      <div className={ styles.ll_block_gallery_horizontal } ref={ horizontalRef }>
        <Swiper
          slidesPerView={1}
          effect={'fade'}
        >
        {
          Object.keys(gallery).map((key, idx) => {
            if ( gallery[key].horizontalImage && gallery[key].horizontalImage.sourceUrl ) {
              const imgUrl = gallery[key].horizontalImage.sourceUrl
              return (
                <SwiperSlide key={idx}>
                  <figure className={ styles.ll_gallery_image }>
                    <figcaption className={ styles.ll_gallery_title }>{ gallery[key].horizontalImage.altText }</figcaption>
                    <Image loader={ () => imgUrl } src={ imgUrl } alt={ gallery[key].horizontalImage.altText } layout="fill" />
                  </figure>
                </SwiperSlide>
              )
            }
          })
        }
        </Swiper>
      </div>
    </div>

  )
}