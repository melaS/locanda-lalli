import styles from './SectionBlockLeft.module.scss'
export default function SectionBlockLeft({ children }) {
  return(
    <div className={ styles.ll_block_sx }>
      { children }
    </div>
  )
}