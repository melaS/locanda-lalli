import styles from './SectionBlockPrice.module.scss'
export default function SectionBlockPrice({ price = '', from= true }) {
  return (
    <div className={ styles.ll_block_price }>
      { from && <span className={ styles.ll_from }>da</span> }
      { price && (
        <span className={ styles.ll_price }>{ price }</span>
      ) }

    </div>
  )
}