import styles from './SectionBlockRight.module.scss'
export default function SectionBlockRight({ children }) {
  return(
    <div className={ styles.ll_block_dx }>
      { children }
    </div>
  )
}