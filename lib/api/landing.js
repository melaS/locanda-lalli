
import { fetchAPI } from "./utils";

/**
 * Get Landing Info
 * @param id
 * @param idType
 * @returns {Promise<*>}
 */
export async function getLandingIntro(id, idType='DATABASE_ID') {
  const data = await fetchAPI(
    `
      query LandingIntroQuery($id: ID!, $idType: PageIdType!) {
        page(id: $id, idType: $idType) {
          landingIntro {
            intro {
              description
              subtitle
              title
              image {
                altText
                title
                sourceUrl
              }
            }
          }
        }
      }
    `,
    {
      variables: {
        id,
        idType
      }
    }
  )
  return data.page.landingIntro.intro
}

/**
 * Get Landing LandingRooms
 * @param id
 * @param idType
 * @returns {Promise<*>}
 */
export async function getLandingRooms(id, idType='DATABASE_ID') {
  const data = await fetchAPI(
    `
    query LandingRoomsQuery($id: ID!, $idType: PageIdType!) {
      page(id: $id, idType: $idType) {
        landingRooms {
          rooms {
            title
            subtitle
            description
            price
            services
            priceList {
              title
              prices
            }
            gallery {
              slide1 {
                horizontalImage {
                  altText
                  sourceUrl
                }
                verticalImage {
                  altText
                  sourceUrl
                }
              }
              slide2 {
                horizontalImage {
                  altText
                  sourceUrl
                }
                verticalImage {
                  altText
                  sourceUrl
                }
              }
            }
          }
        }
      }
    }
    `,
    {
      variables: {
        id,
        idType
      }
    }
  )
  return data.page.landingRooms.rooms
}

/**
 * Get Landing Restaurant
 * @param id
 * @param idType
 * @returns {Promise<*>}
 */
export async function getLandingRestaurant(id, idType='DATABASE_ID') {
  const data = await fetchAPI(
    `
      query LandingRestaurantQuery($id: ID!, $idType: PageIdType!) {
        page(id: $id, idType: $idType) {
          landingRestaurant {
            restaurant {
              title
              subtitle
              description
              price
              gallery {
                slide1 {
                  horizontalImage {
                    title
                    altText
                    sourceUrl
                  }
                  verticalImage {
                    title
                    altText
                    sourceUrl
                  }
                }
                slide2 {
                  horizontalImage {
                    title
                    altText
                    sourceUrl
                  }
                  verticalImage {
                    title
                    altText
                    sourceUrl
                  }
                }
              }
            }
          }
        }
      }
    `,
    {
      variables: {
        id,
        idType
      }
    }
  )
  return data.page.landingRestaurant.restaurant
}

/**
 * Get Landing Takeaway
 * @param id
 * @param idType
 * @returns {Promise<*>}
 */
export async function getLandingTakeaway(id='', idType='DATABASE_ID') {
  const data = await fetchAPI(
    `query LandingTakeawayQuery($id: ID!, $idType: PageIdType!) {
      page(id: $id, idType: $idType) {
        landingTakeaway {
          takeaway {
            title
            subtitle
            description
            price
            services
            gallery {
              slide1 {
                horizontalImage {
                  altText
                  sourceUrl
                }
                verticalImage {
                  altText
                  sourceUrl
                }
              }
              slide2 {
                horizontalImage {
                  altText
                  sourceUrl
                }
                verticalImage {
                  altText
                  sourceUrl
                }
              }
            }
          }
        }
      }
    }
    `,
    {
      variables: {
        id,
        idType
      }
    }
  )
  return data.page.landingTakeaway.takeaway
}

/**
 * Get Landing Call
 * @param id
 * @param idType
 * @returns {Promise<*>}
 */
export async function getLandingCall(id='', idType='DATABASE_ID') {
  const data = await fetchAPI(
    `
    query LandingCallQuery($id: ID!, $idType: PageIdType!) {
      page(id: $id, idType: $idType) {
        landingCall {
          callUs {
            title
            subtitle
            description
          }
        }
      }
    }
    `,
    {
      variables: {
        id,
        idType
      }
    }
  )
  return data.page.landingCall.callUs
}

/**
 * Get Landing About
 * @param id
 * @param idType
 * @returns {Promise<string|string|*>}
 */
export async function getLandingAbout(id='', idType='DATABASE_ID') {
  const data = await fetchAPI(
    `
    query LandingAboutQuery($id: ID!, $idType: PageIdType!) {
      page(id: $id, idType: $idType) {
        landingAbout {
          about {
            title
            subtitle
            description
            image {
              altText
              sourceUrl
            }
          }
        }
      }
    }
    `,
    {
      variables: {
        id,
        idType
      }
    }
  )
  return data.page.landingAbout.about
}

/**
 * Get Landing Location
 * @param id
 * @param idType
 * @returns {Promise<*>}
 */
export async function getLandingLocation(id='', idType='DATABASE_ID') {
  const data = await fetchAPI(
    `
    query LandingLocationQuery($id: ID!, $idType: PageIdType!) {
      page(id: $id, idType: $idType) {
        landingLocation {
          location {
            title
            subtitle
            address
            map {
              lng
              lat
              address
            }
            link {
              url
              title
              target
            }
            contact {
              email
              phone
            }
          }
        }
      }
    }
    `,
    {
      variables: {
        id,
        idType
      }
    }
  )
  return data.page.landingLocation.location
}