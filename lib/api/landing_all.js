import { fetchAPI } from "./utils";

export async function getLandingPage(id = '6', idType = 'DATABASE_ID') {
  const data = await fetchAPI(
    `
      query LandingQuery($id: ID!, $idType: PageIdType!) {
        page(id: $id, idType: $idType) {
          id
          databaseId
          landingInfo {
            subtitle
            title
            preview {
              altText
              sourceUrl
              title
            }
          }
          landingRooms {
            rooms {
              description
              title
              list {
                room1 {
                  description
                  images {
                    imageHorizontal {
                      title
                      altText
                      sourceUrl
                    }
                    imageVertical {
                      title
                      sourceUrl
                      altText
                    }
                  }
                  name
                }
                room2 {
                  description
                  name
                  images {
                    imageVertical {
                      title
                      altText
                      sourceUrl
                    }
                  }
                }
              }
              price
              services
              priceList {
                prices
                title
              }
            }
          }
          landingTakeaway {
            takeaway {
              description
              title
              hamburger {
                hamburger1 {
                  description
                  price
                  title
                  images {
                    imageHorizontal {
                      altText
                      title
                      sourceUrl
                    }
                  }
                }
              }
            }
          }
          landingRestaurant {
            restaurant {
              title
              description
              images {
                imageHorizontal {
                  altText
                  title
                  sourceUrl
                }
                imageVertical {
                  altText
                  title
                  sourceUrl
                }
              }
            }
          }
          landingAbout {
            about {
              description
              subtitle
              title
              image {
                altText
                sourceUrl
              }
            }
          }
          landingCall {
            callUs {
              description
              subtitle
              title
            }
          }
          landingLocation {
            location {
              address
              subtitle
              title
              contact {
                email
                phone
              }
              map {
                city
                country
                countryShort
                latitude
                longitude
                placeId
                postCode
                state
                stateShort
                streetAddress
                streetName
                streetNumber
                zoom
              }
            }
          }
          telephoneNumber {
            telephoneNumber {
              label
              number
            }
          }
          footer {
            footer {
              address {
                indirizzo
                name
                owner
              }
              contacts {
                email
                mobile
                phone
              }
              timetables {
                day
                night
              }
            }
          }
          header {
            header {
              menu
            }
          }
          social {
            social {
              facebook {
                target
                title
                url
              }
              instagram {
                target
                title
                url
              }
              twitter {
                target
                title
                url
              }
              tripadvisor {
                target
                title
                url
              }
            }
          }
        }
      }
    `,
    {
      variables: { id, idType }
    }
  )
  return data.page
}