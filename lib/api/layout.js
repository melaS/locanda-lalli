import { fetchAPI } from "./utils";

/**
 * Get Header
 * @param id
 * @param idType
 * @returns {Promise<*>}
 */
export async function getHeader(id, idType='DATABASE_ID') {
  const data = await fetchAPI(
    `
      query layoutHeaderQuery($id: ID!, $idType: PageIdType!) {
        page(id: $id, idType: $idType) {
          layoutHeader {
            header {
              logo {
                title
                altText
                sourceUrl
              }
              siteName
            }
          }
        }
      }
    `,
    {
      variables: {
        id,
        idType
      }
    }
  )
  return data.page.layoutHeader.header
}

/**
 * Get Footer
 * @param id
 * @param idType
 * @returns {Promise<*>}
 */
export async function getFooter(id, idType='DATABASE_ID') {
  const data = await fetchAPI(
    `
      query LayoutFooterQuery($id: ID!, $idType: PageIdType!) {
        page(id: $id, idType: $idType) {
          layoutFooter {
            footer {
              address {
                title
                name
                owner
                address
              }
              contacts {
                title
                phone
                mobile
                email
              }
              timetables {
                title
                day
                night
              }
              privacy
            }
          }
        }
      }
    `,
    {
      variables: {
        id,
        idType
      }
    }
  )
  return data.page.layoutFooter.footer
}

/**
 * Get Social
 * @param id
 * @param idType
 * @returns {Promise<void>}
 */
export async function getSocial(id, idType='DATABASE_ID') {
  const data = await fetchAPI(
    `
      query LayoutSocialQuery($id: ID!, $idType: PageIdType!) {
        page(id: $id, idType: $idType) {
          social {
            social {
              facebook {
                title
                target
                url
              }
              instagram {
                title
                target
                url
              }
              tripadvisor {
                title
                target
                url
              }
              twitter {
                title
                target
                url
              }
            }
          }
        }
      }
    `,
    {
      variables: {
        id,
        idType
      }
    }
  )
  return data.page.social.social
}

/**
 * Get Telephone Number
 * @param id
 * @param idType
 * @returns {Promise<*>}
 */
export async function getTelephoneNumbers(id= '', idType='DATABASE_ID') {
  const data = await fetchAPI(
    `query LayoutTelephoneNumberQuery($id: ID!, $idType: PageIdType!) {
        page(id: $id, idType: $idType) {
          telephoneNumber {
            telephoneNumber {
              label
              labelBook
              labelOrder
              number
            }
          }
        }
      }
    `,
    {
      variables: {
        id,
        idType
      }
    }
  )

  return data.page.telephoneNumber.telephoneNumber
}

/**
 * Get Telephone Number
 * @param id
 * @param idType
 * @returns {Promise<*>}
 */
export async function getMenu(id= '', idType='DATABASE_ID') {
  const data = await fetchAPI(
    `query LayoutMenuQuery($id: ID!, $idType: PageIdType!) {
        page(id: $id, idType: $idType) {
          layoutMenu {
            menu {
              item1 {
                label
                icon
              }
              item2 {
                label
                icon
              }
              item3 {
                label
                icon
              }
              item4 {
                label
                icon
              }
            }
          }
        }
      }
    `,
    {
      variables: {
        id,
        idType
      }
    }
  )

  return data.page.layoutMenu.menu
}