const apiUrl = process.env.WORDPRESS_API_URL

export async function fetchAPI(query, {variables} = {}) {

  const headers = { 'Content-Type': 'application/json' }

  const res = await fetch(apiUrl, {
    method: 'POST',
    headers,
    body: JSON.stringify({
      query,
      variables
    })
  })

  const json = await res.json()
  if(json.errors) {
    console.error('ERROR: ', json.errors)
    throw new Error('Failed to fetch API')
  }
  return json.data

}