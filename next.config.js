const path = require('path')
const isProd = process.env.NODE_ENV === 'production'

// webpack
// Override css class name in Webpack config with localIndentName to [local]

// sassOptions
// Import globals scss for all scss files
// Prepend @import "./styles/globals/" in all scss files
module.exports = {
  webpack(config, options) {
    config.module.rules[1].oneOf.forEach((moduleLoader, i) => {
      Array.isArray(moduleLoader.use) &&
      moduleLoader.use.forEach((l) => {
        if (l.loader.includes('css-loader') && !l.loader.includes('postcss-loader')) {
          delete l.options.modules.getLocalIdent;
          l.options.modules = {
            ...l.options.modules,
            localIdentName: '[local]',
          };
        }
      });
    });
    return config;
  },
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
    prependData: `@import "./styles/globals.scss";`,
  },
  images: {
    domains: ['api.glnsvg.altervista.org', 'localhost:8888'],
  },
}