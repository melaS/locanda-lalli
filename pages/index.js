import Head from "next/head";
import Layout from "../components/Layout/Layout";
import LandingRooms from "../components/Landing/LandingRooms/LandingRooms";
import LandingIntro from "../components/Landing/LandingIntro/LandingIntro";
import LandingRestaurant from "../components/Landing/LandingRestaurant/LandingRestaurant";
import LandingTakeaway from "../components/Landing/LandingTakeaway/LandingTakeaway";
import LandingAbout from "../components/Landing/LandingAbout/LandingAbout";
import LandingCall from "../components/Landing/LandingCall/LandingCall";
import LandingLocation from "../components/Landing/LandingLocation/LandingLocation";
import { getLandingIntro, getLandingRooms, getLandingRestaurant, getLandingTakeaway, getLandingAbout, getLandingLocation, getLandingCall } from "../lib/api/landing";
import { getHeader, getFooter, getSocial, getTelephoneNumbers, getMenu } from "../lib/api/layout";

const landingId = process.env.LANDING_ID

export default function Home(props) {
  const {
    header,
    footer,
    menu,
    social,
    telephoneNumber,
    landingIntro,
    landingRooms,
    landingRestaurant,
    landingTakeaway,
    landingAbout,
    landingLocation,
    landingCall,
  } = props
  /*console.log('HEADER:::', header)
  console.log('FOOTER:::', footer)
  console.log('MENU:::', menu)
  console.log('SOCIAL:::', social)
  console.log('LANDING INTRO:::', landingIntro)*/
  console.log('TELEPHONE:::', telephoneNumber)
  /*console.log('LANDING ROOMS:::', landingRooms)*/
  /*console.log('LANDING RESTAURANT:::', landingRestaurant)*/
  /*console.log('LANDING TAKEAWAY:::', landingTakeaway)*/
  /*console.log('LANDING ABOUT:::', landingAbout)*/
  /*console.log('LANDING LOCATION:::', landingLocation)*/
  /*console.log('LANDING CALL:::', landingCall)*/
  return (
    <>
      <Head>
        <title>Locanda del buongustaio"</title>
        <meta name="description" content="Locanda Lalli" />
        <meta name="keywords" content="Locanda, Buongustaio" />
      </Head>
      <Layout
        menu={ menu }
        header={ header }
        footer={ footer }
        telephone={ telephoneNumber }
        social={ social }
      >
        <LandingIntro intro={ landingIntro } />
        <LandingRooms rooms={ landingRooms } telephoneNumber={ telephoneNumber } />
        <LandingRestaurant restaurant={ landingRestaurant } telephoneNumber={ telephoneNumber } />
        <LandingTakeaway takeaway={ landingTakeaway } telephoneNumber={ telephoneNumber } />
        <LandingAbout about={ landingAbout } />
        <LandingLocation location={ landingLocation } />
        <LandingCall call={ landingCall } telephoneNumber={ telephoneNumber } />
      </Layout>
    </>

  )
}

export async function getStaticProps(context) {
  const header = await getHeader(landingId)
  const footer = await getFooter(landingId)
  const menu = await getMenu(landingId)
  const landingIntro = await getLandingIntro(landingId)
  const landingRooms = await getLandingRooms(landingId)
  const landingRestaurant = await getLandingRestaurant(landingId)
  const landingTakeaway = await  getLandingTakeaway(landingId)
  const landingAbout = await getLandingAbout(landingId)
  const landingLocation = await getLandingLocation(landingId)
  const landingCall = await getLandingCall(landingId)
  const social = await getSocial(landingId)
  const telephoneNumber = await getTelephoneNumbers(landingId)
  return {
    props: {
      header,
      footer,
      menu,
      landingIntro,
      landingRooms,
      landingRestaurant,
      landingTakeaway,
      landingAbout,
      landingLocation,
      landingCall,
      social,
      telephoneNumber
    }
  }
}
